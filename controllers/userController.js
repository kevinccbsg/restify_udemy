var helpers = require('../config/helperFunctions');
var UserModel = require('../models/userModel');


module.exports = (server) => {
	server.get("/", (req, res, next) => {
		UserModel.find({}, (err, users) => {
			if (err) return helpers.error(res, next, 'There\'s no data in collection', 500);
			helpers.success(res, next, users);
		});	
	});

	server.get("/user/:id", (req, res, next) => {
		req.assert('id', 'Id is required and must be numeric').notEmpty();
		var errors = req.validationErrors();
		if (errors) helpers.error(res, next, errors[0], 400);
		UserModel.findOne({ _id: req.params.id }, (err, user) => {
			if (err) return helpers.error(res, next, 'There\'s no data in collection', 500);
			if ( user === null) {
				return helpers.error(res, next, 'The specify user could not be found', 404);
			}
			return helpers.success(res, next, user);
		});	
	});

	server.post("/user", (req, res, next) => {
		req.assert('first_name', 'First name is required').notEmpty();
		req.assert('last_name', 'Last name is required').notEmpty();
		req.assert('email_address', 'Email is required and must be a valid email').notEmpty().isEmail();
		req.assert('career', 'Carrer must be student, teacher or professor').notEmpty().isIn(['student','teacher','professor']);
		var errors = req.validationErrors();
		if (errors) helpers.error(res, next, errors, 400);
		var user = new UserModel();
		user.first_name = req.params.first_name;
		user.last_name = req.params.last_name;
		user.email_address = req.params.email_address;
		user.career = req.params.career;
		user.save((err) => {
			if (err) return helpers.error(res, next, 'Error saving user to database'+err, 500);

			return helpers.success(res, next, user);
		});
	});

	server.put("/user/:id", (req, res, next) => {
		req.assert('id', 'Id is required and must be numeric').notEmpty();
		var errors = req.validationErrors();
		if (errors) helpers.error(res, next, errors[0], 400);

		UserModel.findOne({ _id: req.params.id }, (err, user) => {
			if (err) return helpers.error(res, next, 'There\'s no data in collection', 500);
			if ( user === null) {
				return helpers.error(res, next, 'The specify user could not be found', 404);
			}
			var updates = req.params;
			//Lo eliminamos porque no queremos que se cambie ya que mongo lo añade
			delete updates.id;
			for (var field in updates) {
				user[field] = updates[field];
			}
			user.save((err) => {
				if (err) return helpers.error(res, next, 'Error saving user to database'+err, 500);

				return helpers.success(res, next, user);
			});
		});
	});

	server.del("/user/:id", (req, res, next) => {
		req.assert('id', 'Id is required and must be numeric').notEmpty();
		var errors = req.validationErrors();
		if (errors) return helpers.error(res, next, errors[0], 400);
		UserModel.findOne({ _id: req.params.id }, (err, user) => {
			if (err) return helpers.error(res, next, 'There\'s no data in collection', 500);
			if ( user === null) {
				return helpers.error(res, next, 'The specify user could not be found', 404);
			}
			user.remove((err) => {
				if (err) return helpers.error(res, next, 'Not remove from database'+err, 500);

				return helpers.success(res, next, user);
			});
		});
	});
}
