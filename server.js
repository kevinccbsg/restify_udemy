var restify = require('restify');
var logger = require('restify-logger');
var setUpController = require('./controllers/setUpController');
var userController = require('./controllers/userController');
var restifyValidator = require('restify-validator');
var mongoose = require('mongoose');
var config = require('./config/DBConections');

var server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});

mongoose.connect(config.getMongoConnection());

setUpController(server, restify, restifyValidator);

userController(server);

logger.format('my-simple-format', ':method :url :status')
server.use(logger('my-simple-format'));

server.listen(8080, function () {
  console.log('%s listening at %s', server.name, server.url);
});
